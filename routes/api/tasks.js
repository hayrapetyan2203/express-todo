var express = require('express');
var router = express.Router();
var db = require('../../db');

/* GET home page. */
router.get('/', function(req, res, next) {
   var qry = `SELECT * FROM tasks;`;
   
   db.query(qry, function (err, results) {
      if (err) {
         // Handle error
         console.error("Error executing query:", err);
         res.status(500).json({ error: 'An error occurred while fetching tasks' });
      } else {
         // Check if there are no tasks
         if (results.length === 0) {
            res.status(200).json({ message: 'No tasks found' });
         } else {
            // Send query results as JSON response
            res.status(200).json(results);
         }
      }
   });
});


router.post('/', function(req, res, next) {
  if (!req.body.name || !req.body.text) {
     // Handle error
     console.error("Missing required field name or text.");
     return res.status(400).json({ error: 'Missing required field name or text.' });
  } 

  var qry = `INSERT INTO tasks(name, text) VALUES (?, ?)`;
  var values = [req.body.name, req.body.text];
   
  db.query(qry, values, function (err, results) {
    if (err) {
        // Handle error
        console.error("Error executing query:", err);
        return res.status(500).json({ error: 'An error occurred while creating task.' });
    } else {
        // Send success message as JSON response
        return res.status(201).json({ message: 'Task successfully created!' });
    }
  });
});

router.put('/', function(req, res, next) {
  if (!req.body.name || !req.body.text || !req.body.id) {
     // Handle error
     console.error("Missing required field name or text.");
     return res.status(400).json({ error: 'Missing required field name or text.' });
  } 

  var qry = `UPDATE tasks SET name = ?, text = ? WHERE id = ?`;
  var values = [req.body.name, req.body.text, req.body.id];

  db.query(qry, values, function (err, results) {
    if (err) {
        // Handle error
        console.error("Error executing query:", err);
        return res.status(500).json({ error: 'An error occurred while updating task.' + err, body : req.body });
    } else {
        // Send success message as JSON response
        return res.status(201).json({ message: 'Task successfully updated!' });
    }
  });
});


router.delete('/', function(req, res, next) {
  if (!req.body.id) {
     // Handle error
     console.error("Missing required field id.");
     return res.status(400).json({ error: 'Missing required field id.' });
  } 

  var qry = `DELETE FROM tasks WHERE id = ?`;
  var values = [req.body.id];
   
  db.query(qry, values, function (err, results) {
    if (err) {
        // Handle error
        console.error("Error executing query:", err);
        return res.status(500).json({ error: 'An error occurred while deleting task.' });
    } else {
        // Send success message as JSON response
        return res.status(200).json({ message: 'Task successfully deleted!' });
    }
  });
});

module.exports = router;

