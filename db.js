const mysql = require('mysql');

// Database connection pool configuration
const pool = mysql.createPool({
   connectionLimit: 10, // Adjust this value as needed
   host: "localhost",
   user: "root",
   password: "password",
   database: "express_todo" // Add your database name here
});

// Export functions to interact with the database
module.exports = {
   query: function(sql, values, callback) {
      pool.getConnection(function(err, connection) {
         if (err) {
            console.error("Error getting database connection:", err);
            callback(err, null);
         } else {
            connection.query(sql, values, function(err, result) {
               connection.release(); // Release the connection back to the pool
               callback(err, result);
            });
         }
      });
   }
};
